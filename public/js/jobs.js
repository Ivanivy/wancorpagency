$(document).ready(function () {
    var tl = new gsap.timeline();
    tl.add(gsap.fromTo(".skeleton.skeleton-rect", { opacity: 0, y: 100 }, { duration: 0.7, opacity: 1, y: 0 }))
    tl.add(gsap.fromTo(".search-section", { opacity: 0 }, { duration: 1, opacity: 1 }));
    setTimeout(function () {
        $(".loading-container").hide();
        $(".jobs-container").append(
            `
            <div class="job-card-item">
                <img class=" " src="./assets/image.jpg" alt="">
                <div class="job-title">Ingénieur en marketing</div>
                <div class="job-date"><span>01-03-2021</span><span><a class="more-info">details</a></span></div>
            </div>
            <div class="job-card-item">
                <img class=" " src="./assets/image2.jpg" alt="">
                <div class="job-title">Receptioniste</div>
                <div class="job-date"><span>01-03-2021</span><span><a class="more-info">details</a></span></div>
            </div>
            <div class="job-card-item">
                <img class=" " src="./assets/image3.jpg" alt="">
                <div class="job-title">Mecanicien</div>
                <div class="job-date"><span>01-03-2021</span><span><a class="more-info">details</a></span></div>
            </div>
            <div class="job-card-item">
                <img class=" " src="./assets/image.jpg" alt="">
                <div class="job-title">Ingénieur en marketing</div>
                <div class="job-date"><span>01-03-2021</span><span><a class="more-info">details</a></span></div>
            </div>
        `
        );
    }, 3000)
});