$(document).ready(function () {
    gsap.registerPlugin(ScrollTrigger);
    var tl = new gsap.timeline();
    tl.add(gsap.fromTo(".custom-jumbotron > div,.custom-jumbotron nav", { opacity: 0 }, { duration: 1, opacity: 1 }));
    tl.add(gsap.fromTo(".custom-jumbotron .message-jumbotron span.colored-text", { width: "0rem" }, { duration: 1.1, width: "10.5rem" }));
    tl.add(gsap.fromTo(".call-to-scroll svg", { y: 0, opacity: 0 }, { duration: 1, y: 20, opacity: 1, repeat: -1 }));

    var tl2 = new gsap.timeline({
        scrollTrigger: {
            trigger: ".presentation-container .description",
            start: "top 50%"
        }
    }
    );
    tl2.add(gsap.fromTo(".presentation-container .description", { duration: 3, opacity: 0 }, { opacity: 1, scrollTrigger: ".presentation-container .description" }));
    tl2.add(gsap.fromTo(".presentation-container .image", { y: "2rem", opacity: 0 }, { duration: 1, y: "0rem", opacity: 1, scrollTrigger: ".presentation-container .description .details>div:nth-child(1)" }));
});