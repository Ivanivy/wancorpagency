//user class correspongind to the one applying for a job
//the constructor doesnt 
//key represent the differents values that cant take the object
//useful if you want to implements api request, the method shoul be just called to get the differents properties

class User {

    constructor(id = null, firstName = null, lastName = null, birthDate = null, profession = null, experienceYears = null) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthDate = birthDate;
        this.profession = profession;
        this.experienceYears = experienceYears;
    }

    getId() {
        return this.id;
    }
    getFirstName() {
        return this.firstName;
    }
    getLastName() {
        return this.lastName;
    }
    getBirthDate() {
        return this.birthDate;
    }
    getProfession() {
        return this.profession;
    }
    getExperienceYears() {
        return this.experienceYears;
    }

    setId(id) {
        this.id = id;
    }

    setFirstName(firstName) {
        this.firstName = firstName;
    }

    setLastName(lastName) {
        this.lastName = lastName;
    }

    setBirthName(birthDate) {
        this.birthDate = birthDate;
    }

    setProfession(profession) {
        this.profession = profession;
    }

    setExperienceYears(experienceYears) {
        this.experienceYears = experienceYears;
    }

    getKeys() {
        return {
            id: "id",
            firstName: "firstName",
            lastName: "lastName",
            birthDate: "brithDate",
            profession: "profession",
            experienceYears: "experienceYears"
        }
    }

    getJSObjet() {
        return {
            [this.getKeys().id]: this.getId(),
            [this.getKeys().firstName]: this.getFirstName(),
            [this.getKeys().lastName]: this.getLastName(),
            [this.getKeys().birthDate]: this.getBirthDate(),
            [this.getKeys().profession]: this.getProfession(),
            [this.getKeys().experienceYears]: this.getExperienceYears(),
        }
    }
}