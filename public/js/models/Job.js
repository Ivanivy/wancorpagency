class Job {
    constructor(id = null, title = null, description = null, requirements = null, url = null, date) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.requirements = requirements;
        this.url = url;
        this.date = date;
    }
    getId() {
        return this.id;
    }

    getTitle() {
        return this.title;
    }

    getDescription() {
        return this.description;
    }

    getRequirements() {
        return this.requirements;
    }

    getUrl() {
        return this.url;
    }

    getDate() {
        return this.date;
    }

    setId(id) {
        this.id = id;
    }

    setTitle(title) {
        this.title = title;
    }

    setDescription(description) {
        this.description = description;
    }

    setRequirements(requirements) {
        this.requirements = requirements;
    }

    setUrl(url) {
        this.url = url;
    }

    setDate(date) {
        this.date = date;
    }

    getKeys() {
        return {
            id: "id",
            title: "title",
            description: "description",
            requirements: "requirements",
            url: "url",
            date: "date"
        }
    }


    getJsonObject() {
        return {
            [this.getKeys().id]: this.getId(),
            [this.getKeys().title]: this.getTitle(),
            [this.getKeys().description]: this.getDescription(),
            [this.getKeys().requirements]: this.getRequirements(),
            [this.getUrl().url]: this.getUrl(),
            [this.getDate().date]: this.getDate()
        }
    }
}