//Application take in parmaeters job and user;
class Application {

    constructor(id = null, user = null, job = null, content = null) {
        this.id = id;
        this.user = user;
        this.job = job;
        this.content = content;
    }

    getId() {
        return this.id;
    }

    getUser() {
        return this.user;
    }
    getJob() {
        return this.job;
    }
    getContent() {
        return this.content;
    }

    setId(id) {
        this.id = id;
    }

    setUser(user) {
        this.user = user;
    }
    setJob(job) {
        this.job = job;
    }
    setContent(content) {
        this.content = content;
    }

    getKeys() {
        return {
            id: "id",
            user: "user",
            job: "job",
            content: "content"
        }
    }

    getJSObject() {
        return {
            [this.getKeys().id]: this.getId(),
            [this.getKeys().user]: this.getUser(),
            [this.getKeys().job]: this.getJob(),
            [this.getKeys().content]: this.getContent(),
        }
    }
}